#!/bin/bash

# Download the backup from the S3 bucket

aws s3 cp s3://my-bucket/hello_world.tar.gz .

# Restore the data for the Hello World application

tar -xf hello_world.tar.gz
