resource "aws_instance" "hello_world" {
  ami = "ami-0123456789abcdef0"
  instance_type = "t2.micro"
  tags = {
    Name = "Hello World"
  }
}
