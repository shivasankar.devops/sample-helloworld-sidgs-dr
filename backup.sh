#!/bin/bash

# Backup the data for the Hello World application

cd /var/www/hello_world
tar -cf hello_world.tar.gz *

# Upload the backup to an S3 bucket

aws s3 cp hello_world.tar.gz s3://my-bucket
